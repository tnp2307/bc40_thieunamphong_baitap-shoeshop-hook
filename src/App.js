
import './App.css';
import ShoeShopMain from './ShoeShop/ShoeShopMain';

function App() {
  return (
    <div>
     <ShoeShopMain/>
    </div>
  );
}

export default App;

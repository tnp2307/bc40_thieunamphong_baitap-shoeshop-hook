import React ,{ useState, useEffect } from 'react'

export default function ShoeCart(props) {
    // const [cartArr, setCart] = useState([]);
    let renderTbody=()=>{
      return props.cart.map((item)=>{
        return(
          <tr>
             <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price * item.quantityBuy}</td>
          <td>
            <img src={item.image} style={{ width: 50 }} alt="" />
          </td>
          <td>
            <button
              className="btn btn-danger"
              onClick={() => props.handleXoa(item.id)}
            >
              -
            </button>
            {item.quantitybuy}
            <button
              className="btn btn-success"
              onClick={() => props.handleThem(item.id)}
            >
              +
            </button>
          </td>
          <td>
            <button
              className="btn btn-danger"
              onClick={() => props.handleXoaAll(item.id)}
            >
              Xóa
            </button>
          </td>
          </tr>
        )
      })
    }

    return (
        <div>
        <table className="table">
          <thead>
            <th>ID</th>
            <th>Name</th>
            <th>Price</th>
            <th>Img</th>
            <th>Qty</th>
            <th>Thao tác</th>
          </thead>
          <tbody>{renderTbody()}</tbody>
        </table>
      </div>
  )
}

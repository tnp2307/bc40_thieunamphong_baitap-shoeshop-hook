import React from "react";

export default function ShoeItem(props) {

  return (
    
      <div className="col-3 py-4">
        <div className="d-flex flex-column">
          <button
            type="button"
            data-toggle="modal"
            data-target="#exampleModalCenter"
          >
            <div className="card">
              <div className="card-body">
                <h5 className="card-title">{props.item.name}</h5>
                <p className="card-text">{props.item.price}</p>
                <img
                  className="card-img-top"
                  src={props.item.image}
                  alt="Card image cap"
                />
              </div>
            </div>
          </button>
          <button
            className="btn btn-primary"
            onClick={() => props.handleAddToCart(props.item)}
          >
            Add to cart
          </button>
        </div>
      </div>
  
  );
}

import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import ShoeCart from "./ShoeCart";
import ShoeList from "./ShoeList";
import { dataShoe } from "./DataShoe";

export default function ShoeShopMain(props) {
  let [shoeArr, setShoe] = useState([]);
  let [cartArr, setCart] = useState([]);

  useEffect(() => {
    setShoe(dataShoe);
  }, []);
  
  let handleAddToCart = (shoe) => {
    
    let newShoe = {...shoe,quantityBuy:1}
    let cloneCart = [...cartArr];
    let vitri = cloneCart.findIndex((item) => item.id == shoe.id);
    if (vitri == -1) {
    cloneCart.push(newShoe);
    setCart(cloneCart)}
    else{
      cloneCart[vitri].quantityBuy++
      setCart(cloneCart)
    }
  };
  let handleThem=(shoeId)=>{
    let cloneCart = [...cartArr];
    let vitri = cloneCart.findIndex((item) => item.id == shoeId);
    cloneCart[vitri].quantityBuy++
    setCart(cloneCart)
  }
  let handleXoa=(shoeId)=>{
    let cloneCart = [...cartArr];
    let vitri = cloneCart.findIndex((item) => item.id == shoeId);
    if (cloneCart[vitri].quantityBuy>1){
      cloneCart[vitri].quantityBuy--}
      else{
        cloneCart.splice(vitri,1)

      }
    setCart(cloneCart)
  }
  let handleXoaAll = (shoeId) => {
    let cloneCart = [...cartArr];
    let viTri = cloneCart.findIndex((item) => item.id == shoeId);
    cloneCart.splice(viTri, 1);
    setCart(cloneCart)
  };

  return (
    <div>
      <div className="bg-warning text-center">Shoe Cart</div>
      <ShoeCart cart={cartArr} handleXoaAll={handleXoaAll} handleXoa={handleXoa} handleThem={handleThem}/>
      <div className="bg-primary text-center">Shoe List</div>
      <ShoeList shoeList={shoeArr} handleAddToCart={handleAddToCart} />
    </div>
  );
}



import {combineReducers} from "redux";
import { shoeShopReducer } from "./ShoeReducer";

export let rootReducer = combineReducers({ shoeShopReducer });

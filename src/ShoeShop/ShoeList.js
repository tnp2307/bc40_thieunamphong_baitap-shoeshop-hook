import React from 'react'
import ShoeItem from './ShoeItem'

export default function ShoeList(props) {
    
    let renderShoeList =()=>{
        return props.shoeList.map((item)=>{
            return <ShoeItem item={item} handleAddToCart={props.handleAddToCart}/>
        })
    }
   
    return (
    <div className='row' >
        
        {renderShoeList()}
    </div>
  )
}
